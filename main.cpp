//
//  main.cpp
//  University
//
//  Created by Gordiy Rushynets on 3/5/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Person
{
protected:
    string name;
    double age;
    string sex;
public:
    void set_name(string person_name)
    {
        name = person_name;
    }
    
    void set_age(double person_age)
    {
        age = person_age;
    }
    
    void set_sex(string person_sex)
    {
        sex = person_sex;
    }
    
    string get_name()
    {
        return name;
    }
    
    double get_age()
    {
        return age;
    }
    
    string get_sex()
    {
        return sex;
    }
    
    friend ostream &operator << (ostream &out, Person &person)
    {
        return out << person.name << ' ' << person.age << ' ' << person.sex;
    }
    
    friend istream &operator >> (istream &in, Person &person)
    {
        return in >> person.name >> person.age >> person.sex;
    }
};

class Student : public Person
{
private:
    string faculty;
    double grade;
public:
    void set_faculty(string student_faculty)
    {
        faculty = student_faculty;
    }
    
    void set_grade(double student_grade)
    {
        grade = student_grade;
    }
    
    string get_faculty()
    {
        return faculty;
    }
    
    double get_grade()
    {
        return grade;
    }
    
    friend ostream &operator << (ostream &out, Student &student)
    {
        return out << student.faculty << ' ' << student.grade;
    }
    
    friend istream &operator >> (istream &in, Student &student)
    {
        return in >> student.faculty >> student.grade;
    }
};

class Professor : public Person
{
private:
    string post;
    int skill;
public:
    void set_post(string professor_post)
    {
        post = professor_post;
    }
    
    void set_skill(int professor_skill)
    {
        skill = professor_skill;
    }
    
    string get_post()
    {
        return post;
    }
    
    int get_skill()
    {
        return skill;
    }
    
    friend ostream &operator << (ostream &out, Professor &prof)
    {
        return out << prof.post << ' ' << prof.skill;
    }
    
    friend istream &operator >> (istream &in, Professor &prof)
    {
        return in >> prof.post >> prof.skill;
    }
};


int main(int argc, const char * argv[]) {
    Student *student;
    
    ifstream fin_students("/Users/gordiy/cpp/University/University/students.txt");
    ifstream fin_professors("/Users/gordiy/cpp/University/University/professor.txt");
    
    int count_students;
    fin_students >> count_students;
    
    student = new Student[count_students];
    
    for(int i = 0; i < count_students; i++)
    {
        string student_name;
        double age;
        string sex;
        string faculty;
        double grade;
        
        fin_students >> student_name;
        fin_students >> age;
        fin_students >> sex;
        fin_students >> faculty;
        fin_students >> grade;
        
        student[i].set_name(student_name);
        student[i].set_age(age);
        student[i].set_sex(sex);
        student[i].set_faculty(faculty);
        student[i].set_grade(grade);
    }
    
    return 0;
}
